#!/bin/bash

SERVER_NAME=$1

if [ "x${SERVER_NAME}" = "x" ]; then
  echo "server name is mandatory"
  exit 1
fi

export CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=3333 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"

[ ! -f ${DYNAMO_ROOT}/Tomcat/apache-tomcat-7.0.37/lib/mysql-connector-java-5.1.15-bin.jar ] \
  && cp  ${DYNAMO_ROOT}/DAS/lib/mysql-connector-java-5.1.15-bin.jar ${DYNAMO_ROOT}/Tomcat/apache-tomcat-7.0.37/lib

${DYNAMO_HOME}/bin/startDynamoOnTomcat -s ${SERVER_NAME} -m $(grep module ${DYNAMO_HOME}/servers/${SERVER_NAME}/localconfig/moduleList.properties | awk -F'=' '{print$2}' | tr ',' ':' | sed 's/ //g')

exit 0
