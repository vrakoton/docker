#!/bin/bash

# start by sourcing the ENDECA env files, if they exists
if [ -f "/opt/endeca/MDEX/6.5.1/mdex_setup_sh.ini" ]; then
  . /opt/endeca/MDEX/6.5.1/mdex_setup_sh.ini
fi

if [ -f "/opt/endeca/PlatformServices/workspace/setup/installer_sh.ini" ]; then
  . /opt/endeca/PlatformServices/workspace/setup/installer_sh.ini
fi

# start the platform
function start_platform {
  echo "starting the platform service"
  cd ${ENDECA_ROOT}/tools/server/bin
  ./startup.sh
}

# Stop the platform
function stop_platform {
 echo "stopping the platform service"
  cd ${ENDECA_ROOT}/tools/server/bin
  ./shutdown.sh 
}

# starts the tools and framework part
function start_tools {
  echo "starting the tools and framework"
  cd ${ENDECA_TOOLS_ROOT}/server/bin
  ./startup.sh &
}

# stops the tools and framework server
function stop_tools {
  echo "stopping the tools and framework"
  cd ${ENDECA_TOOLS_ROOT}/server/bin
  ./shutdown.sh &
}

# starts the CAS service
function start_cas {
  echo "starting the CAS service"
  cd ${CAS_ROOT}/bin
  ./cas-service.sh &
}

# stops the CAS service
function stop_cas {
 echo "stoping the CAS service"
  cd ${CAS_ROOT}/bin
  ./cas-service-shutdown.sh & 
}

function help {
  echo "usage: endeca.sh start|stop <application name>"
  echo ""
  echo "application name can be : platform, tools, cas or all"
}

case $1 in
  start)
    case $2 in
      all)
        start_platform
        start_tools
        start_cas
        ;;

      platform)
        start_platform
        ;;

      tools)
        start_tools
        ;;

      cas)
        start_cas
        ;;

      *) help;;
    esac
  ;;
  stop)
    case $2 in
      cas)
        stop_cas
        ;;
      
      platform)
        stop_platform
        ;;

      tools)
        stop_tools
        ;;

      all)
        echo "not implemented"
        ;;
    esac
  ;;
  *) help;;
esac