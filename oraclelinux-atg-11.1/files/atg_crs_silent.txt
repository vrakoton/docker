# Wed Mar 16 16:11:53 UTC 2016
# Replay feature output
# ---------------------
# This file was built by the Replay feature of InstallAnywhere.
# It contains variables that were set by Panels, Consoles or Custom Code.



#Choose Install Folder
#---------------------
USER_INSTALL_DIR=/opt/atg/ATG11.1

#Install
#-------
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/.CommerceReferenceStore_config/config/setup_rule.xml=Yes
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/.CommerceReferenceStore_config/lib/vendorlibs.jar=Yes
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/.CommerceReferenceStore_config/lib/OCIAPlatformlib.jar=Yes
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/.CommerceReferenceStore_config/bin/getlicense.sh=Yes
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/.CommerceReferenceStore_config/bin/getlicense.bat=Yes
-fileOverwrite_/opt/atg/ATG11.1/CommerceReferenceStore/uninstall/.CommerceReferenceStore_uninstall/Uninstall Oracle Commerce Reference Store 11.1.lax=Yes
