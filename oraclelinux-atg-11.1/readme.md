#Endeca Experience Manager and MDEX 11.1

These are docker files and configuration files to build an Oracle Guided Search environment for version 11.1.

##Disclaimer

You may reuse or customize these files at your own risk. I or RE ENTER SAS can not be liable for any damages due to the use of these containers on your integration or production environment.

Oracle does not currently support Oracle Commerce or Oracle Guided Search run in container.

##Building and running Experience Manager

To build the Experience Manager part, issue the following command:

`docker build -f xpmanager-11.1-dockerfile -t reenter/oraclelinux-xpamanager-11.1 .` 

or just use the helper script I wrote:

`sudo ./build.sh xpmanager`

This will build a container running:

- MDEX
- Endeca Platform
- Endeca Tools and Framework
- Endeca Content Aquisition Service (CAS)

To run the container, use:

`docker run -d -p 8006:8006 -p 15000:15000 -h xpmanager.local.dockerA.docker.local --name xpmanager -v /opt/data/docker-runtime/endeca/xpmanager/Apps:/opt/endeca/Apps -v /opt/data/docker-runtime/endeca/share:/srv/endeca/application_export_archive --dns 172.17.42.1 --dns 8.8.8.8 --dns-search local.dockerA.docker.local reenter/oraclelinux-xpamanager-11.1`

##Building and running MDEX

To build the MDEX container, run the following command:

`docker build -f mdex-11.1-dockerfile -t reenter/oraclelinux-mdex-11.1 .` 

or just use the helper script I wrote:

`sudo ./build.sh mdex`

To run the container, use:

`docker run -d -h mdexlive.local.dockerA.docker.local --name mdexlive -p 8888:8888 -p 8090:8090 --dns 172.17.42.1 --dns 8.8.8.8 --dns-search local.dockerA.docker.local reenter/oraclelinux-mdex-11.1`