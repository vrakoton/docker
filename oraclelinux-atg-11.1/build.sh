#!/bin/bash

# return codes
OK=0
N_OK=-1

installers_folder="installers"

function get_installer () {
  output=$1
  source=$2

  if [ "x${output}" == "x" ]; then
    echo "output file name should not be empty"
    exit $N_OK
  fi

  if [ ! -f "${installers_folder}/${output}" ]; then
    if [ "x${source}" == "x" ]; then
      echo "Dowload source can not be empty"
      exit $N_OK
    fi

    echo "Downloading ${output} from ${source}..."
    wget ${source} --no-check-certificate -O "${installers_folder}/${output}"
  fi
}

# ----------------------------------------------------
# usage function to print the script usage options
# ----------------------------------------------------
function usage() {
  echo "usage: build.sh <container type>"
  echo "  container type can be one of:"
  echo "    - mdex: to build an MDEX container"
  echo "    - xpmanager: to build an experience manager container"
  echo ""
}


# check current user is root as docker can only be run as root
#current_user=`whoami`
#if [ "${current_user}" != "root" ]; then
#  echo "You need to run this script as root"
#  exit $N_OK
#fi

# create the folder containing the installers
if [ ! -d "${installers_folder}" ]; then
  mkdir "${installers_folder}"
fi

# dowload pre-requisite installers
get_installer "jdk-7u75-linux-x64.rpm" "https://www.bizlauncher-it.com/installers/jdk/jdk-7u75-linux-x64.rpm"
get_installer "Endeca_MDEX-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/Endeca_MDEX-11.1.zip"
get_installer "Endeca_Platform_Services-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/Endeca_Platform_Services-11.1.zip"
get_installer "Endeca_Tools_and_Framework-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/Endeca_Tools_and_Framework-11.1.zip"
get_installer "Endeca_CAS-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/Endeca_CAS-11.1.zip"
get_installer "ATG_Platform-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/ATG_Platform-11.1.zip"
get_installer "ATG_ServiceCenter-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/ATG_ServiceCEnter-11.1.zip"
get_installer "ATG_CommerceReferenceStore-11.1.zip" "https://www.bizlauncher-it.com/installers/ATG11.1/ATG_CommerceReferenceStore-11.1.zip"

CONTAINER_TOBUILD="mdex"

if [ "x$1" != "x" ]; then
  CONTAINER_TOBUILD=$1
fi

case ${CONTAINER_TOBUILD} in
  mdex)
    echo "Building mdex..."
    DOCKER_FILE="mdex-11.1-dockerfile"
    DOCKER_NAME="reenter/oraclelinux-mdex:11.1"
  ;;
  xpmanager)
    echo "Building Experience Manager..."
    DOCKER_FILE="xpmanager-11.1-dockerfile"
    DOCKER_NAME="reenter/oraclelinux-xpamanager:11.1"
  ;;
  atg)
    echo "Building ATG"
    DOCKER_FILE="atg-11.1-dockerfile"
    DOCKER_NAME="reenter/oraclelinux-atg:11.1"
  ;;
  *)
    echo "Unknow container type"
    usage
    exit $N_OK
  ;;
esac

if [ "x${DOCKER_NAME}" == "x" ]; then
  exit $N_OK
fi

if [ "x${DOCKER_FILE}" == "x" ]; then
  exit $N_OK
fi

docker build --rm -t ${DOCKER_NAME} -f ${DOCKER_FILE} .
