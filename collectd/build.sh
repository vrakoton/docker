#!/bin/bash

current=`pwd`
cd etc/ && tar -cf collectd.tar *
cd ${current}
docker build -t reenter/collectd-5.4.0 .