#Introduction

This folder contains docker files to create base containers based on Oracle Linux. ATG and Endeca installations need extra software to be installed
to work. Yum install command takes ages to execute because of cache refresh so we try to earn some time by using our own containers with preinstalled
utilities.