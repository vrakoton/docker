#!/bin/bash

myuser=`whoami`

if [ "${myuser}" != "root" ]; then
  echo "You need to be root to run this script, either login as root or use sudo"
  exit 1
fi

if [ ! -f Dockerfile ]; then
  echo "Can not find the Dockerfile"
  exit 1
fi

if [ ! -d temp ]; then
  mkdir temp
fi

tar -czf temp/scripts.tar scripts && docker build -t reenter/oraclelinux-weblogic-12c .

exit 0