#!/usr/bin/env /opt/oracle/Middleware/wlserver/common/bin/wlst.sh
from java.util import Properties
from java.io import FileInputStream

# ===============
#  Variable Defs
# ===============
print('Reading property file: /opt/scripts/weblogic.properties')
prop = Properties()
prop.load(FileInputStream('/opt/scripts/weblogic.properties'))

ADMIN_SERVER        = prop.getProperty('ADMIN_SERVER')
ADMIN_SERVER_PORT     = int(prop.getProperty('ADMIN_SERVER_PORT'))
ADMIN_SERVER_SSLPORT     = int(prop.getProperty('ADMIN_SERVER_SSLPORT'))
DATABASE          = prop.getProperty('DATABASE')
DOMAIN            = prop.getProperty('DOMAIN')
DOMAIN_HOME         = prop.getProperty('DOMAIN_HOME')
LISTEN_ADDRESS        = prop.getProperty('LISTEN_ADDRESS')
MACHINE           = prop.getProperty('MACHINE')
MANAGED_SERVER        = prop.getProperty('MANAGED_SERVER')
MANAGED_SERVER_PORT     = int(prop.getProperty('MANAGED_SERVER_PORT'))
MDS_USER          = prop.getProperty('MDS_USER')
MDS_PASSWORD        = prop.getProperty('MDS_PASSWORD')
MW_HOME           = prop.getProperty('MW_HOME')
NODE_MANAGER        = prop.getProperty('NODE_MANAGER')
NODE_MANAGER_LISTEN_ADDRESS = prop.getProperty('NODE_MANAGER_LISTEN_ADDRESS')
NODE_MANAGER_PASSWORD = prop.getProperty('NODE_MANAGER_PASSWORD')
NODE_MANAGER_PORT     = int(prop.getProperty('NODE_MANAGER_PORT'))
OSB_HOME              = prop.getProperty('OSB_HOME')
SOAINFRA_USER         = prop.getProperty('SOAINFRA_USER')
SOAINFRA_PASSWORD     = prop.getProperty('SOAINFRA_PASSWORD')
WEBLOGIC_PASSWORD     = prop.getProperty('WEBLOGIC_PASSWORD')
WL_HOME               = prop.getProperty('WL_HOME')
ORACLE_HOST           = prop.getProperty('ORACLE_HOST')
ORACLE_PORT           = prop.getProperty('ORACLE_PORT')
ORACLE_SID           = prop.getProperty('ORACLE_SID')

#=======================================================================================
# Configure the Administration Server and SSL port.
#
# To enable access by both local and remote processes, you should not set the 
# listen address for the server instance (that is, it should be left blank or not set). 
# In this case, the server instance will determine the address of the machine and 
# listen on it. 
#=======================================================================================
readDomain(DOMAIN_HOME)

print('Creating server', MANAGED_SERVER)
cd('/')
create(MANAGED_SERVER, 'Server')
cd('/Server/' + MANAGED_SERVER)
cmo.setListenAddress(LISTEN_ADDRESS)
cmo.setListenPort(MANAGED_SERVER_PORT)

#=======================================================================================
# Write the domain and close the domain template.
#=======================================================================================

print('Persisting changes!')
updateDomain()
closeDomain()

#=======================================================================================
# Exit WLST.
#=======================================================================================

print('Server creation done')
exit()