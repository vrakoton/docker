#!/usr/bin/env /opt/oracle/Middleware/wlserver/common/bin/wlst.sh
from java.util import Properties
from java.io import FileInputStream

# ===============
#  Variable Defs
# ===============
print('Reading property file: /opt/scripts/weblogic.properties')
prop = Properties()
prop.load(FileInputStream('/opt/scripts/weblogic.properties'))

ADMIN_SERVER        = prop.getProperty('ADMIN_SERVER')
ADMIN_SERVER_PORT     = int(prop.getProperty('ADMIN_SERVER_PORT'))
ADMIN_SERVER_SSLPORT     = int(prop.getProperty('ADMIN_SERVER_SSLPORT'))
DATABASE          = prop.getProperty('DATABASE')
DOMAIN            = prop.getProperty('DOMAIN')
DOMAIN_HOME         = prop.getProperty('DOMAIN_HOME')
LISTEN_ADDRESS        = prop.getProperty('LISTEN_ADDRESS')
MACHINE           = prop.getProperty('MACHINE')
MANAGED_SERVER        = prop.getProperty('MANAGED_SERVER')
MANAGED_SERVER_PORT     = int(prop.getProperty('MANAGED_SERVER_PORT'))
MDS_USER          = prop.getProperty('MDS_USER')
MDS_PASSWORD        = prop.getProperty('MDS_PASSWORD')
MW_HOME           = prop.getProperty('MW_HOME')
NODE_MANAGER        = prop.getProperty('NODE_MANAGER')
NODE_MANAGER_LISTEN_ADDRESS = prop.getProperty('NODE_MANAGER_LISTEN_ADDRESS')
NODE_MANAGER_PASSWORD = prop.getProperty('NODE_MANAGER_PASSWORD')
NODE_MANAGER_PORT     = int(prop.getProperty('NODE_MANAGER_PORT'))
OSB_HOME              = prop.getProperty('OSB_HOME')
SOAINFRA_USER         = prop.getProperty('SOAINFRA_USER')
SOAINFRA_PASSWORD     = prop.getProperty('SOAINFRA_PASSWORD')
WEBLOGIC_PASSWORD     = prop.getProperty('WEBLOGIC_PASSWORD')
WL_HOME               = prop.getProperty('WL_HOME')
ORACLE_HOST           = prop.getProperty('ORACLE_HOST')
ORACLE_PORT           = prop.getProperty('ORACLE_PORT')
ORACLE_SID           = prop.getProperty('ORACLE_SID')

#=======================================================================================
# Configure the Administration Server and SSL port.
#
# To enable access by both local and remote processes, you should not set the 
# listen address for the server instance (that is, it should be left blank or not set). 
# In this case, the server instance will determine the address of the machine and 
# listen on it. 
#=======================================================================================
readTemplate(WL_HOME + '/common/templates/wls/wls.jar')

print('Setting admin server port to ', ADMIN_SERVER_PORT)
cd('Servers/AdminServer')
set('ListenAddress','')
set('ListenPort', ADMIN_SERVER_PORT)

print('Setting admin server port to ', ADMIN_SERVER_SSLPORT)
create('AdminServer','SSL')
cd('SSL/AdminServer')
set('Enabled', 'True')
set('ListenPort', ADMIN_SERVER_SSLPORT)

#=======================================================================================
# Define the user password for weblogic.
#=======================================================================================

print('Setting weblogic user password')
cd('/')
cd('Security/base_domain/User/weblogic')
cmo.setPassword(WEBLOGIC_PASSWORD)

#=======================================================================================
# Write the domain and close the domain template.
#=======================================================================================
print('Persisting changes!')
setOption('OverwriteDomain', 'true')
writeDomain(DOMAIN_HOME)
closeTemplate()

#=======================================================================================
# Exit WLST.
#=======================================================================================

print('Domain creation done')
exit()