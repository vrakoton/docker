#!/usr/bin/env /opt/oracle/Middleware/wlserver/common/bin/wlst.sh
from java.util import Properties
from java.io import FileInputStream

# ===============
#  Variable Defs
# ===============
print('Reading property file: /opt/scripts/weblogic.properties')
prop = Properties()
prop.load(FileInputStream('/opt/scripts/weblogic.properties'))

ADMIN_SERVER        = prop.getProperty('ADMIN_SERVER')
ADMIN_SERVER_PORT     = int(prop.getProperty('ADMIN_SERVER_PORT'))
ADMIN_SERVER_SSLPORT     = int(prop.getProperty('ADMIN_SERVER_SSLPORT'))
DATABASE          = prop.getProperty('DATABASE')
DOMAIN            = prop.getProperty('DOMAIN')
DOMAIN_HOME         = prop.getProperty('DOMAIN_HOME')
LISTEN_ADDRESS        = prop.getProperty('LISTEN_ADDRESS')
MACHINE           = prop.getProperty('MACHINE')
MANAGED_SERVER        = prop.getProperty('MANAGED_SERVER')
MANAGED_SERVER_PORT     = int(prop.getProperty('MANAGED_SERVER_PORT'))
MDS_USER          = prop.getProperty('MDS_USER')
MDS_PASSWORD        = prop.getProperty('MDS_PASSWORD')
MW_HOME           = prop.getProperty('MW_HOME')
NODE_MANAGER        = prop.getProperty('NODE_MANAGER')
NODE_MANAGER_LISTEN_ADDRESS = prop.getProperty('NODE_MANAGER_LISTEN_ADDRESS')
NODE_MANAGER_PASSWORD = prop.getProperty('NODE_MANAGER_PASSWORD')
NODE_MANAGER_PORT     = int(prop.getProperty('NODE_MANAGER_PORT'))
OSB_HOME              = prop.getProperty('OSB_HOME')
WL_HOME               = prop.getProperty('WL_HOME')

#=======================================================================================
# Configure the Administration Server and SSL port.
#
# To enable access by both local and remote processes, you should not set the 
# listen address for the server instance (that is, it should be left blank or not set). 
# In this case, the server instance will determine the address of the machine and 
# listen on it. 
#=======================================================================================
readDomain(DOMAIN_HOME)

print('Creating data sources')
cd('/')

# ATG_CORE
create('ATG_CORE', 'JDBCSystemResource')
cd('JDBCSystemResource/ATG_CORE/JdbcResource/ATG_CORE')
create('myJdbcDriverParams','JDBCDriverParams')
cd('JDBCDriverParams/NO_NAME_0')
set('DriverName','oracle.jdbc.xa.client.OracleXADataSource')
set('URL','jdbc:oracle:thin:@' + DATABASE)
set('PasswordEncrypted', 'ATG_CORE')

create('myProps','Properties')
cd('Properties/NO_NAME_0')
create('user', 'Property')
cd('Property/user')
cmo.setValue('ATG_CORE')

cd('/JDBCSystemResource/ATG_CORE/JdbcResource/ATG_CORE')
create('myJdbcDataSourceParams','JDBCDataSourceParams')
cd('JDBCDataSourceParams/NO_NAME_0')
set('JNDIName', java.lang.String("jdbc/ATG_CORE"))

cd('/JDBCSystemResource/ATG_CORE/JdbcResource/ATG_CORE')
create('myJdbcConnectionPoolParams','JDBCConnectionPoolParams')
cd('JDBCConnectionPoolParams/NO_NAME_0')
set('TestTableName','SYSTABLES')
set('InitialCapacity', 10)
set('MinCapacity', 10)
set('MaxCapacity', 30)

# ATG_SWA (Switching data source A)
cd('/')
create('ATG_SWA', 'JDBCSystemResource')
cd('JDBCSystemResource/ATG_SWA/JdbcResource/ATG_SWA')
create('myJdbcDriverParams','JDBCDriverParams')
cd('JDBCDriverParams/NO_NAME_0')
set('DriverName','oracle.jdbc.xa.client.OracleXADataSource')
set('URL','jdbc:oracle:thin:@' + DATABASE)
set('PasswordEncrypted', 'ATG_SWA')

create('myProps','Properties')
cd('Properties/NO_NAME_0')
create('user', 'Property')
cd('Property/user')
cmo.setValue('ATG_SWA')

cd('/JDBCSystemResource/ATG_SWA/JdbcResource/ATG_SWA')
create('myJdbcDataSourceParams','JDBCDataSourceParams')
cd('JDBCDataSourceParams/NO_NAME_0')
set('JNDIName', java.lang.String("jdbc/ATG_SWA"))

cd('/JDBCSystemResource/ATG_SWA/JdbcResource/ATG_SWA')
create('myJdbcConnectionPoolParams','JDBCConnectionPoolParams')
cd('JDBCConnectionPoolParams/NO_NAME_0')
set('TestTableName','SYSTABLES')
set('InitialCapacity', 10)
set('MinCapacity', 10)
set('MaxCapacity', 30)

# ATG_SWB (Switching data source B)
cd('/')
create('ATG_SWB', 'JDBCSystemResource')
cd('JDBCSystemResource/ATG_SWB/JdbcResource/ATG_SWB')
create('myJdbcDriverParams','JDBCDriverParams')
cd('JDBCDriverParams/NO_NAME_0')
set('DriverName','oracle.jdbc.xa.client.OracleXADataSource')

set('URL','jdbc:oracle:thin:@' + DATABASE)
set('PasswordEncrypted', 'ATG_SWB')

create('myProps','Properties')
cd('Properties/NO_NAME_0')
create('user', 'Property')
cd('Property/user')
cmo.setValue('ATG_SWB')

cd('/JDBCSystemResource/ATG_SWB/JdbcResource/ATG_SWB')
create('myJdbcDataSourceParams','JDBCDataSourceParams')
cd('JDBCDataSourceParams/NO_NAME_0')
set('JNDIName', java.lang.String("jdbc/ATG_SWB"))

cd('/JDBCSystemResource/ATG_SWB/JdbcResource/ATG_SWB')
create('myJdbcConnectionPoolParams','JDBCConnectionPoolParams')
cd('JDBCConnectionPoolParams/NO_NAME_0')
set('TestTableName','SYSTABLES')
set('InitialCapacity', 10)
set('MinCapacity', 10)
set('MaxCapacity', 30)

#=======================================================================================
# Target resources to the servers. 
#=======================================================================================

print('Assigning resources')
cd('/')
assign('JDBCSystemResource', 'ATG_CORE', 'Target', MANAGED_SERVER)
assign('JDBCSystemResource', 'ATG_SWA', 'Target', MANAGED_SERVER)
assign('JDBCSystemResource', 'ATG_SWB', 'Target', MANAGED_SERVER)

print('Persisting changes!')
updateDomain()
closeDomain()

#=======================================================================================
# Exit WLST.
#=======================================================================================

print('Datasource creation done')
exit()