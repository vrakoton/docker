# Jenkins container

This is a container build file for Jenkins which is used for RE ENTER SAS. We need the jenkins user used in the original container to be part of the docker group to be able to launch Docker containers during builds.

You may need to change the docker group id depending on the one used on your host.

To build the container, use:

```
docker build -t registry.re-enter.fr/jenkins:2.7.4 .
```

To run the container with Docker support (i.e. with the ability to launch a container from within a buiild), you need to use:

```
docker run -d --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):$(which docker) --name jenkins --env JENKINS_OPTS="--prefix=/jenkins" -p 8080:8080 -v /srv/jenkins:/var/jenkins_home registry.re-enter.fr/jenkins:2.7.4
```