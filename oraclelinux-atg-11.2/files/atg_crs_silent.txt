# Wed Sep 07 15:23:59 UTC 2016
# Replay feature output
# ---------------------
# This file was built by the Replay feature of InstallAnywhere.
# It contains variables that were set by Panels, Consoles or Custom Code.



#Choose Install Folder
#---------------------
USER_INSTALL_DIR=/opt/ATG11.2

#Install
#-------
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/.CommerceReferenceStore_config/config/setup_rule.xml=Yes
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/.CommerceReferenceStore_config/lib/vendorlibs.jar=Yes
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/.CommerceReferenceStore_config/lib/OCIAPlatformlib.jar=Yes
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/.CommerceReferenceStore_config/bin/getlicense.sh=Yes
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/.CommerceReferenceStore_config/bin/getlicense.bat=Yes
-fileOverwrite_/opt/ATG11.2/CommerceReferenceStore/uninstall/.CommerceReferenceStore_uninstall/Uninstall Oracle Commerce Reference Store 11.2.lax=Yes
