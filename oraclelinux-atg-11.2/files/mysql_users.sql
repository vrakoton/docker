create database ATG_CORE character set utf8;
create database ATG_PUB character set utf8;
create database ATG_SWA character set utf8;
create database ATG_SWB character set utf8;
create database ATG_AGENT character set utf8;

grant all privileges on ATG_CORE.* to atg identified by 'atg';
grant all privileges on ATG_PUB.* to atg identified by 'atg';
grant all privileges on ATG_SWA.* to atg identified by 'atg';
grant all privileges on ATG_SWB.* to atg identified by 'atg';
grant all privileges on ATG_AGENT.* to atg identified by 'atg';