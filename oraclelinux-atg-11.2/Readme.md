# Introduction

# About the containers

## ATG Platform and Commerce Reference Store 11.2 container

The ATG Platform container contains:

- ATG Platform
- ATG Commerce Service Center
- ATG Commerce Reference Store

To build the container containing the full blown ATG installation issue the following command:

```bash
docker build -t reenter/oraclelinux-atg-11.2 -f atg-11.2-dockerfile . 
```

To run the container:

```bash
docker run --link mysql-atg-11.2 reenter/oraclelinux-atg-11.2 
```

**Note**: For all user login (admin, merchandising, csc service etc...) password is: **atg11.2Docker**


## MySQL container

You need to name the mysql container in order to be able to connect to it from the ATG container.

```bash
docker run --name mysql-atg-11.2 -e MYSQL_ROOT_PASSWORD=reenter.com -d
```